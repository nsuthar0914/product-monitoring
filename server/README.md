# Oxehealth Product Usage Monitor Server
Serverside implementation for providing rest apis for the monitoring dashboard. It provided two endpoints -
    - /api/cs-monitoring - customer support specific room wise monitoring data
    - /api/sales-monitoring - sales specific facility wise monitoring data

## Installation
`virtualenv venv`
`source venv/bin/activate`
`pip install -r requirements.txt`

## Setup sample data
`python sample_data.py`

## Run Server
`python app.py`

Server runs on [http://localhost:5000](http://localhost:5000) by default.

## Run Tests
`pytest`