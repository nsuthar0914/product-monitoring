from datetime import date

def test_cs_monitoring(client):
    """Test information about the transcriber library on the backend is provided"""
    response = client.get('/api/cs-monitoring')
    assert len(response.json) == 4
    assert response.status_code == 200

def test_sales_monitoring(client):
    """Test information about the transcriber library on the backend is provided"""
    response = client.get('/api/sales-monitoring')
    assert response.json == {'alert_frequency': 1.0, 'recent_uptime': date.today().strftime("%Y-%m-%d")}
    assert response.status_code == 200
