from datetime import date
from logic import merge_room_event_info, get_roomwise_info
from sample_data import EVENTS_SMALL


def test_merge_room_event_info_downtime():
    info = {"alert_counts": [], "uptimes": {}, "alert_frequency": 0, "recent_uptime": date.today()}
    event = {'room': 'room01', 'metric': 'system:offline', 'date': '2020-08-14', 'count': '1'}
    merged_info = merge_room_event_info(info, event)
    assert merged_info == {'alert_counts': [], 'alert_frequency': 0, 'recent_uptime': date.today(), 'uptimes': {}}

def test_merge_room_event_info_uptime():
    info = {'alert_counts': [], 'alert_frequency': 0, 'recent_uptime': date.today(), 'uptimes': {}}
    event = {'room': 'room01', 'metric': 'system:pause', 'date': '2020-08-14', 'count': '0'}
    merged_info = merge_room_event_info(info, event)
    assert merged_info == {'alert_counts': [], 'alert_frequency': 0, 'recent_uptime': '2020-08-14', 'uptimes': {'system:pause': '2020-08-14'}}

def test_merge_room_event_info_alert():
    info = {'alert_counts': [], 'alert_frequency': 0, 'recent_uptime': '2020-08-14', 'uptimes': {'system:pause': '2020-08-14'}}
    event = {'room': 'room01', 'metric': 'gui:alert:raised', 'date': '2020-08-14', 'count': '1'}
    merged_info = merge_room_event_info(info, event)
    assert merged_info == {'alert_counts': [1], 'alert_frequency': 1.0, 'recent_uptime': '2020-08-14', 'uptimes': {'system:pause': '2020-08-14'}}

def test_merge_room_event_info_alert_frequency():
    info = {'alert_counts': [1], 'alert_frequency': 1.0, 'recent_uptime': '2020-08-14', 'uptimes': {'system:pause': '2020-08-14'}}
    event = {'room': 'room01', 'metric': 'gui:alert:raised', 'date': '2020-08-15', 'count': '2'}
    merged_info = merge_room_event_info(info, event)
    assert merged_info == {'alert_counts': [1, 2], 'alert_frequency': 1.5, 'recent_uptime': '2020-08-14', 'uptimes': {'system:pause': '2020-08-14'}}

def test_get_roomwise_events():
    roomwise_info = get_roomwise_info(EVENTS_SMALL)
    assert roomwise_info == {
        'room01': {
            'alert_counts': [],
            'uptimes': {},
            'alert_frequency': 0,
            'recent_uptime': date.today()
        },
        'room02': {
            'alert_counts': [],
            'uptimes': {},
            'alert_frequency': 0,
            'recent_uptime': date.today()
        },
        'room03': {
            'alert_counts': [],
            'uptimes': {},
            'alert_frequency': 0,
            'recent_uptime': date.today()
        },
        'room04': {
            'alert_counts': [1],
            'uptimes': {},
            'alert_frequency': 1.0,
            'recent_uptime': date.today()
        }
    }