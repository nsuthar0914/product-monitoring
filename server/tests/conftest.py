import os
import pytest
from app import create_app
from sample_data import add_data, EVENTS_SMALL


basedir = os.path.abspath(os.path.dirname(__file__))

TESTDB_PATH = os.path.join(basedir, 'test.db')
TEST_DATABASE_URI = 'sqlite:////' + TESTDB_PATH
test_config = {
    'SQLALCHEMY_DATABASE_URI': TEST_DATABASE_URI,
    'SQLALCHEMY_TRACK_MODIFICATIONS': False,
    'DEBUG': True,
    'TESTING': True,
}


@pytest.fixture(scope='module')
def client():
    test_app = add_data(test_config, "tests/test.db", EVENTS_SMALL)
    with test_app.test_client() as c:
        yield c