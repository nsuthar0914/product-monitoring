import os
from uuid import uuid4
from app import create_app, db, dev_config
from models import Event
from datetime import date

basedir = os.path.abspath(os.path.dirname(__file__))

# Data to initialize database with
EVENTS_SMALL = [
    {'room': 'room01', 'metric': 'system:offline', 'date': '2020-08-14', 'count': '1'},
    {'room': 'room02', 'metric': 'system:paused',  'date': '2020-08-14', 'count': '1'},
    {'room': 'room03','metric': 'system:maintenance',  'date': '2020-08-14', 'count': '1'},
    {'room': 'room04','metric': 'gui:alert:raised',  'date': '2020-08-14', 'count': '1'}
]

EVENTS = [
  {
    "room": "room01",
    "metric": "system:offline",
    "date": "2020-08-21",
    "count": 0
  },
  {
    "room": "room02",
    "metric": "system:offline",
    "date": "2020-08-21",
    "count": 0
  },
  {
    "room": "room03",
    "metric": "system:offline",
    "date": "2020-08-21",
    "count": 864
  },
  {
    "room": "room04",
    "metric": "system:offline",
    "date": "2020-08-21",
    "count": 0
  },
  {
    "room": "room01",
    "metric": "system:offline",
    "date": "2020-08-20",
    "count": 1728
  },
  {
    "room": "room02",
    "metric": "system:offline",
    "date": "2020-08-20",
    "count": 0
  },
  {
    "room": "room03",
    "metric": "system:offline",
    "date": "2020-08-20",
    "count": 0
  },
  {
    "room": "room04",
    "metric": "system:offline",
    "date": "2020-08-20",
    "count": 0
  },
  {
    "room": "room01",
    "metric": "system:offline",
    "date": "2020-08-19",
    "count": 0
  },
  {
    "room": "room02",
    "metric": "system:offline",
    "date": "2020-08-19",
    "count": 864
  },
  {
    "room": "room03",
    "metric": "system:offline",
    "date": "2020-08-19",
    "count": 0
  },
  {
    "room": "room04",
    "metric": "system:offline",
    "date": "2020-08-19",
    "count": 0
  },
  {
    "room": "room01",
    "metric": "system:offline",
    "date": "2020-08-18",
    "count": 42336
  },
  {
    "room": "room02",
    "metric": "system:offline",
    "date": "2020-08-18",
    "count": 0
  },
  {
    "room": "room03",
    "metric": "system:offline",
    "date": "2020-08-18",
    "count": 0
  },
  {
    "room": "room04",
    "metric": "system:offline",
    "date": "2020-08-18",
    "count": 0
  },
  {
    "room": "room01",
    "metric": "system:offline",
    "date": "2020-08-17",
    "count": 30240
  },
  {
    "room": "room02",
    "metric": "system:offline",
    "date": "2020-08-17",
    "count": 0
  },
  {
    "room": "room03",
    "metric": "system:offline",
    "date": "2020-08-17",
    "count": 0
  },
  {
    "room": "room04",
    "metric": "system:offline",
    "date": "2020-08-17",
    "count": 0
  },
  {
    "room": "room01",
    "metric": "system:offline",
    "date": "2020-08-16",
    "count": 1728
  },
  {
    "room": "room02",
    "metric": "system:offline",
    "date": "2020-08-16",
    "count": 0
  },
  {
    "room": "room03",
    "metric": "system:offline",
    "date": "2020-08-16",
    "count": 0
  },
  {
    "room": "room04",
    "metric": "system:offline",
    "date": "2020-08-16",
    "count": 0
  },
  {
    "room": "room01",
    "metric": "system:offline",
    "date": "2020-08-15",
    "count": 2592
  },
  {
    "room": "room02",
    "metric": "system:offline",
    "date": "2020-08-15",
    "count": 0
  },
  {
    "room": "room03",
    "metric": "system:offline",
    "date": "2020-08-15",
    "count": 864
  },
  {
    "room": "room04",
    "metric": "system:offline",
    "date": "2020-08-15",
    "count": 0
  },
  {
    "room": "room01",
    "metric": "system:offline",
    "date": "2020-08-14",
    "count": 0
  },
  {
    "room": "room02",
    "metric": "system:offline",
    "date": "2020-08-14",
    "count": 0
  },
  {
    "room": "room03",
    "metric": "system:offline",
    "date": "2020-08-14",
    "count": 0
  },
  {
    "room": "room04",
    "metric": "system:offline",
    "date": "2020-08-14",
    "count": 0
  },
  {
    "room": "room01",
    "metric": "system:paused",
    "date": "2020-08-21",
    "count": 1800
  },
  {
    "room": "room02",
    "metric": "system:paused",
    "date": "2020-08-21",
    "count": 4500
  },
  {
    "room": "room03",
    "metric": "system:paused",
    "date": "2020-08-21",
    "count": 900
  },
  {
    "room": "room04",
    "metric": "system:paused",
    "date": "2020-08-21",
    "count": 0
  },
  {
    "room": "room01",
    "metric": "system:paused",
    "date": "2020-08-20",
    "count": 900
  },
  {
    "room": "room02",
    "metric": "system:paused",
    "date": "2020-08-20",
    "count": 0
  },
  {
    "room": "room03",
    "metric": "system:paused",
    "date": "2020-08-20",
    "count": 1800
  },
  {
    "room": "room04",
    "metric": "system:paused",
    "date": "2020-08-20",
    "count": 0
  },
  {
    "room": "room01",
    "metric": "system:paused",
    "date": "2020-08-19",
    "count": 0
  },
  {
    "room": "room02",
    "metric": "system:paused",
    "date": "2020-08-19",
    "count": 4500
  },
  {
    "room": "room03",
    "metric": "system:paused",
    "date": "2020-08-19",
    "count": 2700
  },
  {
    "room": "room04",
    "metric": "system:paused",
    "date": "2020-08-19",
    "count": 5400
  },
  {
    "room": "room01",
    "metric": "system:paused",
    "date": "2020-08-18",
    "count": 0
  },
  {
    "room": "room02",
    "metric": "system:paused",
    "date": "2020-08-18",
    "count": 0
  },
  {
    "room": "room03",
    "metric": "system:paused",
    "date": "2020-08-18",
    "count": 900
  },
  {
    "room": "room04",
    "metric": "system:paused",
    "date": "2020-08-18",
    "count": 0
  },
  {
    "room": "room01",
    "metric": "system:paused",
    "date": "2020-08-17",
    "count": 4500
  },
  {
    "room": "room02",
    "metric": "system:paused",
    "date": "2020-08-17",
    "count": 0
  },
  {
    "room": "room03",
    "metric": "system:paused",
    "date": "2020-08-17",
    "count": 1800
  },
  {
    "room": "room04",
    "metric": "system:paused",
    "date": "2020-08-17",
    "count": 0
  },
  {
    "room": "room01",
    "metric": "system:paused",
    "date": "2020-08-16",
    "count": 900
  },
  {
    "room": "room02",
    "metric": "system:paused",
    "date": "2020-08-16",
    "count": 900
  },
  {
    "room": "room03",
    "metric": "system:paused",
    "date": "2020-08-16",
    "count": 900
  },
  {
    "room": "room04",
    "metric": "system:paused",
    "date": "2020-08-16",
    "count": 0
  },
  {
    "room": "room01",
    "metric": "system:paused",
    "date": "2020-08-15",
    "count": 11700
  },
  {
    "room": "room02",
    "metric": "system:paused",
    "date": "2020-08-15",
    "count": 3600
  },
  {
    "room": "room03",
    "metric": "system:paused",
    "date": "2020-08-15",
    "count": 0
  },
  {
    "room": "room04",
    "metric": "system:paused",
    "date": "2020-08-15",
    "count": 900
  },
  {
    "room": "room01",
    "metric": "system:paused",
    "date": "2020-08-14",
    "count": 4500
  },
  {
    "room": "room02",
    "metric": "system:paused",
    "date": "2020-08-14",
    "count": 0
  },
  {
    "room": "room03",
    "metric": "system:paused",
    "date": "2020-08-14",
    "count": 900
  },
  {
    "room": "room04",
    "metric": "system:paused",
    "date": "2020-08-14",
    "count": 0
  },
  {
    "room": "room01",
    "metric": "system:maintenance",
    "date": "2020-08-21",
    "count": 0
  },
  {
    "room": "room02",
    "metric": "system:maintenance",
    "date": "2020-08-21",
    "count": 0
  },
  {
    "room": "room03",
    "metric": "system:maintenance",
    "date": "2020-08-21",
    "count": 0
  },
  {
    "room": "room04",
    "metric": "system:maintenance",
    "date": "2020-08-21",
    "count": 0
  },
  {
    "room": "room01",
    "metric": "system:maintenance",
    "date": "2020-08-20",
    "count": 0
  },
  {
    "room": "room02",
    "metric": "system:maintenance",
    "date": "2020-08-20",
    "count": 0
  },
  {
    "room": "room03",
    "metric": "system:maintenance",
    "date": "2020-08-20",
    "count": 0
  },
  {
    "room": "room04",
    "metric": "system:maintenance",
    "date": "2020-08-20",
    "count": 0
  },
  {
    "room": "room01",
    "metric": "system:maintenance",
    "date": "2020-08-19",
    "count": 0
  },
  {
    "room": "room02",
    "metric": "system:maintenance",
    "date": "2020-08-19",
    "count": 0
  },
  {
    "room": "room03",
    "metric": "system:maintenance",
    "date": "2020-08-19",
    "count": 0
  },
  {
    "room": "room04",
    "metric": "system:maintenance",
    "date": "2020-08-19",
    "count": 0
  },
  {
    "room": "room01",
    "metric": "system:maintenance",
    "date": "2020-08-18",
    "count": 10800
  },
  {
    "room": "room02",
    "metric": "system:maintenance",
    "date": "2020-08-18",
    "count": 0
  },
  {
    "room": "room03",
    "metric": "system:maintenance",
    "date": "2020-08-18",
    "count": 0
  },
  {
    "room": "room04",
    "metric": "system:maintenance",
    "date": "2020-08-18",
    "count": 0
  },
  {
    "room": "room01",
    "metric": "system:maintenance",
    "date": "2020-08-17",
    "count": 0
  },
  {
    "room": "room02",
    "metric": "system:maintenance",
    "date": "2020-08-17",
    "count": 0
  },
  {
    "room": "room03",
    "metric": "system:maintenance",
    "date": "2020-08-17",
    "count": 0
  },
  {
    "room": "room04",
    "metric": "system:maintenance",
    "date": "2020-08-17",
    "count": 0
  },
  {
    "room": "room01",
    "metric": "system:maintenance",
    "date": "2020-08-16",
    "count": 0
  },
  {
    "room": "room02",
    "metric": "system:maintenance",
    "date": "2020-08-16",
    "count": 0
  },
  {
    "room": "room03",
    "metric": "system:maintenance",
    "date": "2020-08-16",
    "count": 900
  },
  {
    "room": "room04",
    "metric": "system:maintenance",
    "date": "2020-08-16",
    "count": 0
  },
  {
    "room": "room01",
    "metric": "system:maintenance",
    "date": "2020-08-15",
    "count": 0
  },
  {
    "room": "room02",
    "metric": "system:maintenance",
    "date": "2020-08-15",
    "count": 0
  },
  {
    "room": "room03",
    "metric": "system:maintenance",
    "date": "2020-08-15",
    "count": 0
  },
  {
    "room": "room04",
    "metric": "system:maintenance",
    "date": "2020-08-15",
    "count": 0
  },
  {
    "room": "room01",
    "metric": "system:maintenance",
    "date": "2020-08-14",
    "count": 0
  },
  {
    "room": "room02",
    "metric": "system:maintenance",
    "date": "2020-08-14",
    "count": 0
  },
  {
    "room": "room03",
    "metric": "system:maintenance",
    "date": "2020-08-14",
    "count": 0
  },
  {
    "room": "room04",
    "metric": "system:maintenance",
    "date": "2020-08-14",
    "count": 0
  },
  {
    "room": "room01",
    "metric": "gui:alert:raised",
    "date": "2020-08-21",
    "count": 22
  },
  {
    "room": "room02",
    "metric": "gui:alert:raised",
    "date": "2020-08-21",
    "count": 5
  },
  {
    "room": "room03",
    "metric": "gui:alert:raised",
    "date": "2020-08-21",
    "count": 14
  },
  {
    "room": "room04",
    "metric": "gui:alert:raised",
    "date": "2020-08-21",
    "count": 48
  },
  {
    "room": "room01",
    "metric": "gui:alert:raised",
    "date": "2020-08-20",
    "count": 6
  },
  {
    "room": "room02",
    "metric": "gui:alert:raised",
    "date": "2020-08-20",
    "count": 2
  },
  {
    "room": "room03",
    "metric": "gui:alert:raised",
    "date": "2020-08-20",
    "count": 6
  },
  {
    "room": "room04",
    "metric": "gui:alert:raised",
    "date": "2020-08-20",
    "count": 47
  },
  {
    "room": "room01",
    "metric": "gui:alert:raised",
    "date": "2020-08-19",
    "count": 230
  },
  {
    "room": "room02",
    "metric": "gui:alert:raised",
    "date": "2020-08-19",
    "count": 1
  },
  {
    "room": "room03",
    "metric": "gui:alert:raised",
    "date": "2020-08-19",
    "count": 18
  },
  {
    "room": "room04",
    "metric": "gui:alert:raised",
    "date": "2020-08-19",
    "count": 22
  },
  {
    "room": "room01",
    "metric": "gui:alert:raised",
    "date": "2020-08-18",
    "count": 290
  },
  {
    "room": "room02",
    "metric": "gui:alert:raised",
    "date": "2020-08-18",
    "count": 2
  },
  {
    "room": "room03",
    "metric": "gui:alert:raised",
    "date": "2020-08-18",
    "count": 13
  },
  {
    "room": "room04",
    "metric": "gui:alert:raised",
    "date": "2020-08-18",
    "count": 47
  },
  {
    "room": "room01",
    "metric": "gui:alert:raised",
    "date": "2020-08-17",
    "count": 99
  },
  {
    "room": "room02",
    "metric": "gui:alert:raised",
    "date": "2020-08-17",
    "count": 0
  },
  {
    "room": "room03",
    "metric": "gui:alert:raised",
    "date": "2020-08-17",
    "count": 20
  },
  {
    "room": "room04",
    "metric": "gui:alert:raised",
    "date": "2020-08-17",
    "count": 38
  },
  {
    "room": "room01",
    "metric": "gui:alert:raised",
    "date": "2020-08-16",
    "count": 16
  },
  {
    "room": "room02",
    "metric": "gui:alert:raised",
    "date": "2020-08-16",
    "count": 3
  },
  {
    "room": "room03",
    "metric": "gui:alert:raised",
    "date": "2020-08-16",
    "count": 17
  },
  {
    "room": "room04",
    "metric": "gui:alert:raised",
    "date": "2020-08-16",
    "count": 6
  },
  {
    "room": "room01",
    "metric": "gui:alert:raised",
    "date": "2020-08-15",
    "count": 1
  },
  {
    "room": "room02",
    "metric": "gui:alert:raised",
    "date": "2020-08-15",
    "count": 5
  },
  {
    "room": "room03",
    "metric": "gui:alert:raised",
    "date": "2020-08-15",
    "count": 5
  },
  {
    "room": "room04",
    "metric": "gui:alert:raised",
    "date": "2020-08-15",
    "count": 6
  },
  {
    "room": "room01",
    "metric": "gui:alert:raised",
    "date": "2020-08-14",
    "count": 25
  },
  {
    "room": "room02",
    "metric": "gui:alert:raised",
    "date": "2020-08-14",
    "count": 1
  },
  {
    "room": "room03",
    "metric": "gui:alert:raised",
    "date": "2020-08-14",
    "count": 20
  },
  {
    "room": "room04",
    "metric": "gui:alert:raised",
    "date": "2020-08-14",
    "count": 24
  }
]

def add_data(config=dev_config(), db_name='sample.db', events=EVENTS):
    if os.path.exists(db_name):
        os.remove(db_name)
    app = create_app(config)
    with app.app_context():
        # Iterate over the EVENTS structure and populate the database
        for event in events:
            e = Event()
            e.room = event["room"]
            e.metric= event["metric"]
            e.count = event["count"]
            e.event_id = str(uuid4())
            dateitems = event["date"].split('-')
            e.date = date(int(dateitems[0]), int(dateitems[1]), int(dateitems[2]))
            db.session.add(e)
        db.session.commit()
    return app

if __name__ == '__main__':
    add_data()