import os
import connexion
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_cors import CORS

basedir = os.path.abspath(os.path.dirname(__file__))

ma = Marshmallow()
db = SQLAlchemy()

def dev_config():
    return {
        'SQLALCHEMY_ECHO': True,
        'SQLALCHEMY_DATABASE_URI': 'sqlite:////' + os.path.join(basedir, 'sample.db'),
        'SQLALCHEMY_TRACK_MODIFICATIONS': False
    }

def create_app(config=dev_config()):
    # Create the Connexion application instance
    connex_app = connexion.App(__name__, specification_dir=basedir)
    connex_app.add_api('swagger.yml')
    app = connex_app.app
    app.config.update(config)
    db.init_app(app)
    ma.init_app(app)
    CORS(app)
    app.config['CORS_HEADERS'] = 'Content-Type'
    with app.app_context():
        db.create_all()
    return app

# If we're running in stand alone mode, run the application
if __name__ == '__main__':
    app = create_app()
    app.run(host='0.0.0.0', port=5000, debug=True)
