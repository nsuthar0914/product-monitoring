from app import db
from models import (
    Event,
    EventSchema,
)

def get_events():
    """
    This function fetches the raw data from db and provides it to logic layer

    :return:        list of dicts of events
    """
    # Create the list of events from our data
    event = Event.query.all()

    # Serialize the data for the response
    event_schema = EventSchema(many=True)
    return event_schema.dump(event)