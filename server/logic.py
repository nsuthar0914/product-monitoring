from datetime import date


def merge_room_event_info(info, event):
    """Function to aggregate over raw events data. Ideally this could be done in db.
    There was some confusion around how the uptime should be defined.
    Currently, we are just going over events list sorted by decreasin date
    and taking up the first 0 count from the downtime like event as uptime
    Args:
        info ([dict]): [existing monitoring info dict for room or facility]
        event ([dict]): [new event to be merged]

    Returns:
        [dict]: [Updated info dict]
    """
    event_count = int(event["count"])
    if event["metric"] == "gui:alert:raised":
        info["alert_counts"].append(event_count)
        info["alert_frequency"] = sum(info["alert_counts"])/len(info["alert_counts"])
    elif event_count == 0:
        if event["metric"] not in info["uptimes"]:
            info["uptimes"][event["metric"]] = event["date"]
            info["recent_uptime"] = max(info["uptimes"].values())
    return info

def get_roomwise_info(events):
    """Looping through each event in a list of raw events
    and generating a roomwise info dict 

    Args:
        events ([list(dicts)]): [list of event dicts from event model]

    Returns:
        [dict]: [Room wise aggregated data in the form of a single dict]
    """
    roomwise_info = {}
    for event in events:
        if event["room"] in roomwise_info:
            roomwise_info[event["room"]] = merge_room_event_info(roomwise_info[event["room"]], event)
        else:
            roomwise_info[event["room"]] = merge_room_event_info({"alert_counts": [], "uptimes": {}, "alert_frequency": 0, "recent_uptime": date.today()}, event)
    return roomwise_info


# Create a handler for customer support info (GET)
def cs_monitoring():
    """This function responds to a request for /api/cs-monitoring
    with the roomwise aggregated uptime and alert frequency info

    Returns:
        [list(dict)]: [list of dicts of type {room, recent_uptime, alert_frequency}]
    """
    from data import get_events
    roomwise_info_dict = get_roomwise_info(get_events())
    roomwise_info_list = []
    for (key, value) in roomwise_info_dict.items():
        roomwise_info_list.append({
            "room": key,
            "recent_uptime": value["recent_uptime"],
            "alert_frequency": value["alert_frequency"],
        })
    return roomwise_info_list

# Create a handler for sales info (GET)
def sales_monitoring():
    """This function responds to a request for /api/sales-monitoring
    with the facility wise aggregated uptime and alert frequency info

    Returns:
        [dict]: [dict of type {recent_uptime, alert_frequency}]
    """
    from data import get_events
    roomwise_info_dict = get_roomwise_info(get_events())
    facility_info_dict = {"recent_uptimes":[], "alert_frequencies": []}
    for (key, value) in roomwise_info_dict.items():
        facility_info_dict["recent_uptimes"].append(value["recent_uptime"])
        facility_info_dict["alert_frequencies"].append(value["alert_frequency"])
    return {
        "recent_uptime": max(facility_info_dict["recent_uptimes"]),
        "alert_frequency": sum(facility_info_dict["alert_frequencies"]),
    }