from datetime import datetime
from app import db, ma

class Event(db.Model):
    __tablename__ = 'event'
    event_id = db.Column(db.String(32), primary_key=True)
    room = db.Column(db.String(32), index=True)
    metric = db.Column(db.String(32), index=True)
    count = db.Column(db.BigInteger)
    date = db.Column(db.Date)

class EventSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Event
        load_instance = True