# Oxehealth Product Usage Monitor Client
Clientside implementation for providing the monitoring dashboard. It provided two tabs -
    - /cs-monitoring - customer support specific room wise monitoring info
    - /sales-monitoring - sales specific facility wise monitoring info

## Installation
`yarn install`

## Run Server
`yarn start`

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## Run Tests
`yarn test`
