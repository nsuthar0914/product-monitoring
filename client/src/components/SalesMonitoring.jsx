import React, { useEffect, useState } from "react";

const SalesMonitoring = () => {
    const [salesInfo, setSalesInfo] = useState(null);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);
    useEffect(() => {
        setLoading(true);
        const xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState === 4) {
                if (this.status === 200) {
                    setSalesInfo(JSON.parse(this.response));
                    setLoading(false);
                    setError(null);
                } else {
                    setError("Server error, please refresh after some time!");
                    setLoading(false);
                }
            }
        }
        xhttp.open("GET", "http://localhost:5000/api/sales-monitoring");
        xhttp.send();
    }, []);
    return (
        <section>
            <h1>Sales Monitoring</h1>
            {error ? <div>{error}</div> : null}
            {loading ? <div>Loading...</div> : null}
            {salesInfo
                ? <div>
                    <h2>Facility X</h2>
                    <h2>Alert Frequency: {salesInfo.alert_frequency}</h2>
                    <h2>Recent Uptime: {salesInfo.recent_uptime}</h2>
                </div>
                : null}
        </section>
    );
};

export default SalesMonitoring;
