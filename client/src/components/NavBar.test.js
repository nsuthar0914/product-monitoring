import React from 'react';
import { fireEvent, render, waitFor } from '@testing-library/react';
import NavBar from './NavBar';
import { MemoryRouter, Redirect, Route, Switch } from 'react-router-dom';

test('Customer Support link', async () => {
  const { getByText, queryByText } = render(<MemoryRouter>
    <NavBar />
    <Switch>
      <Route path="/cs-monitoring">
        <div>on cs monitoring</div>
      </Route>
      <Route path="/sales-monitoring">
        <div>on sales monitoring</div>
      </Route>
      <Route path="/">
        <Redirect to="/sales-monitoring" />
      </Route>
    </Switch>
  </MemoryRouter>);
  expect(getByText("on sales monitoring")).toBeTruthy();
  expect(queryByText("on cs monitoring")).toBeFalsy();
  const linkElement = getByText(/Customer Support/i);
  expect(linkElement).toBeInTheDocument();
  fireEvent.click(linkElement);
  await waitFor(() => {
    expect(getByText("on cs monitoring")).toBeTruthy();
    expect(queryByText("on sales monitoring")).toBeFalsy();
  });
});

test('Sales link', async () => {
  const { getByText, queryByText } = render(<MemoryRouter>
    <NavBar />
    <Switch>
      <Route path="/cs-monitoring">
        <div>on cs monitoring</div>
      </Route>
      <Route path="/sales-monitoring">
        <div>on sales monitoring</div>
      </Route>
      <Route path="/">
        <Redirect to="/cs-monitoring" />
      </Route>
    </Switch>
  </MemoryRouter>);
  expect(getByText("on cs monitoring")).toBeTruthy();
  expect(queryByText("on sales monitoring")).toBeFalsy();
  const linkElement = getByText(/Sales/i);
  expect(linkElement).toBeInTheDocument();
  fireEvent.click(linkElement);
  await waitFor(() => {
    expect(getByText("on sales monitoring")).toBeTruthy();
    expect(queryByText("on cs monitoring")).toBeFalsy();
  });
});
