import React, { useEffect, useState } from "react";
import "./CustomerSupportMonitoring.css";

const RoomInfo = ({room, alert_frequency, recent_uptime}) => (
    <div key={room} className="roomCard">
        <h3>{room.toUpperCase()}</h3>
        <p>Alert Frequency: {alert_frequency}</p>
        <p>Recent Uptime: {recent_uptime}</p>
    </div>
);

const CustomerSupportMonitoring = () => {
    const [csInfo, setCsInfo] = useState([]);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);
    useEffect(() => {
        setLoading(true);
        const xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState === 4) {
                if (this.status === 200) {
                    setCsInfo(JSON.parse(this.response));
                    setLoading(false);
                    setError(null);
                } else {
                    setError("Server error, please refresh after some time!");
                    setLoading(false);
                }
            }
        };
        xhttp.open("GET", "http://localhost:5000/api/cs-monitoring");
        xhttp.send();
    }, []);
    return (
        <section>
            <h1>Customer Support Monitoring</h1>
            {error ? <div>{error}</div> : null}
            {loading ? <div>Loading...</div> : null}
            {csInfo.length
                ? <div className="roomCardList">{csInfo.map(room => <RoomInfo {...room} />)}</div>
                : null}
        </section>
    );
};

export default CustomerSupportMonitoring;
