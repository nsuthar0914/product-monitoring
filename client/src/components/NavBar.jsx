import React from "react";
import {Link} from "react-router-dom";
import "./NavBar.css";

const NavBar = () => (
    <nav className="navbar">
        <ul className="navbarList">
            <li className="navbarItem">
                <img src="https://assets-global.website-files.com/5f5678688f40811eed0fe944/5f567ffab62bdbcdf297fd25_oxehealth%20logo.svg" />
            </li>
            <li className="navbarItem">
                <Link to="/cs-monitoring">Customer Support</Link>
            </li>
            <li className="navbarItem">
                <Link to="/sales-monitoring">Sales</Link>
            </li>
        </ul>
    </nav>
);

export default NavBar;
