import React from 'react';
import { render, waitFor } from '@testing-library/react';
import CustomerSupportMonitoring from './CustomerSupportMonitoring';
const MockXMLHttpRequest = require('mock-xmlhttprequest');

test('Success', async () => {
    const MockXhr = MockXMLHttpRequest.newMockXhr();
    // Mock JSON response
    MockXhr.onSend = (xhr) => {
        const responseHeaders = { 'Content-Type': 'application/json' };
        const response = JSON.stringify([{'room': "room01", 'alert_frequency': 1.0, 'recent_uptime': "1-1-1"}]);
        xhr.respond(200, responseHeaders, response);
    };
    
    // Install in the global context so "new XMLHttpRequest()" uses the XMLHttpRequest mock
    global.XMLHttpRequest = MockXhr;
    const { getByText, queryByText } = render(<CustomerSupportMonitoring />);
    expect(getByText("Customer Support Monitoring")).toBeTruthy();
    expect(getByText("Loading...")).toBeTruthy();
    await waitFor(() => {
        expect(queryByText("Loading...")).toBeFalsy();
        expect(getByText(/ROOM01/)).toBeTruthy();
        expect(getByText(/1-1-1/)).toBeTruthy();
    });
});
test('Failure', async () => {
    const MockXhr = MockXMLHttpRequest.newMockXhr();
    // Mock JSON response
    MockXhr.onSend = (xhr) => {
        const responseHeaders = { 'Content-Type': 'application/json' };
        const response = JSON.stringify({'error': "server error"});
        xhr.respond(500, responseHeaders, response);
    };
    
    // Install in the global context so "new XMLHttpRequest()" uses the XMLHttpRequest mock
    global.XMLHttpRequest = MockXhr;
    const { getByText, queryByText } = render(<CustomerSupportMonitoring />);
    expect(getByText("Customer Support Monitoring")).toBeTruthy();
    expect(getByText("Loading...")).toBeTruthy();
    await waitFor(() => {
        expect(queryByText("Loading...")).toBeFalsy();
        expect(getByText("Server error, please refresh after some time!")).toBeTruthy();
    });
});