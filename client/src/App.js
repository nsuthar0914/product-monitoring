import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";
import CustomerSupportMonitoring from './components/CustomerSupportMonitoring';
import SalesMonitoring from './components/SalesMonitoring';
import NavBar from './components/NavBar';


function App() {
  return (
    <Router>
      <div>
        <NavBar />

        <Switch>
          <Route path="/cs-monitoring">
            <CustomerSupportMonitoring />
          </Route>
          <Route path="/sales-monitoring">
            <SalesMonitoring />
          </Route>
          <Route path="/">
            <Redirect to="/cs-monitoring" />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
